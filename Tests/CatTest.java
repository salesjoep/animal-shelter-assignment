import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class CatTest {
    private Cat cat = new Cat("Ms. Meow", Gender.Female, "Scratches couch");

    @Test
    public void TestInitialize(){
        this.cat = new Cat("Ms. Meow", Gender.Female, "Scratches couch");
    }

    @Test
    public void TestConstructor(){
        Assert.assertEquals("Ms. Meow", this.cat.Name);
        Assert.assertEquals(Gender.Female, this.cat.Gender);
        Assert.assertNull(this.cat.ReservedBy);
        Assert.assertEquals("Scratches couch", this.cat.BadHabits);
    }

    @Test
    public void TestReservation(){
        Assert.assertNull(this.cat.ReservedBy);
        Assert.assertTrue(this.cat.Reserve("John Doe"));
        Assert.assertNotNull(this.cat.ReservedBy);
        Assert.assertEquals("John Doe", this.cat.ReservedBy.Name);
        Assert.assertFalse(this.cat.Reserve("Jane Doe"));
    }

}