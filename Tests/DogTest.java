import org.junit.Assert;
import org.junit.Test;

import java.time.LocalDate;

import static org.junit.Assert.*;

public class DogTest {
    private Dog dog = new Dog("Sgt. Woof", Gender.Male);

    @Test
    public void TestConstructor(){
        Assert.assertEquals("Sgt. Woof", this.dog.Name);
        Assert.assertEquals(Gender.Male, this.dog.Gender);
        Assert.assertNull(this.dog.ReservedBy);
        Assert.assertEquals(LocalDate.now(), this.dog.LastWalk);
        Assert.assertFalse(this.dog.NeedsWalk());
    }

    @Test
    public void TestReservation(){
        Assert.assertNull(this.dog.ReservedBy);
        Assert.assertTrue(this.dog.Reserve("John Doe"));
        Assert.assertNotNull(this.dog.ReservedBy);
        Assert.assertEquals("John Doe", this.dog.ReservedBy.Name);
        Assert.assertFalse(this.dog.Reserve("Jane Doe"));
    }

}