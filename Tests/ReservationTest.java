import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class ReservationTest {
    private Reservation reservation = new Reservation();
    private List<Animal> testList = new ArrayList<Animal>();

    @Test
    public void TestNewCat() {
        //Assert.assertEquals(null, this.reservation.Animals);
        Assert.assertEquals(testList, this.reservation.Animals);
        this.reservation.NewCat("Ms. Meow", Gender.Female, "Scratches couch");
        Assert.assertEquals(1, this.reservation.Animals.size());
    }

    @Test
    public void TestNewDog() {
        Assert.assertEquals(testList, this.reservation.Animals);
        this.reservation.NewDog("Sgt. Woof", Gender.Male);
        Assert.assertEquals(1, this.reservation.Animals.size());
    }
}