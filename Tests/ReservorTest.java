import org.junit.Assert;
import org.junit.Test;

import java.time.LocalDate;

import static org.junit.Assert.*;

public class ReservorTest {

    @Test
    public void TestConstructor(){
        LocalDate reservedAt = LocalDate.now();
        Reservor reservor = new Reservor("John Doe", reservedAt);
        Assert.assertEquals("John Doe", reservor.Name);
        Assert.assertEquals(reservedAt, reservor.ReservedAt);
    }
}