public class Cat extends Animal {
    public String BadHabits;
    public String getBadHabits(){return BadHabits;}
    private void setBadHabits(String value){BadHabits = value;}

    public double maxPrice = 350;
    public double minPrice = 35;
    private int badHabitsLength;
    public double currentPrice = 350;


    public Cat(String name, Gender gender, String badHabits){
        super(name, gender);
        this.BadHabits = badHabits;
    }

    public Boolean CalculatePrice(){
        Integer multiplier;
        multiplier = this.Name.length() * 20;
        currentPrice -= multiplier;

        if(currentPrice > maxPrice){
            return false;
        }
        if(currentPrice < minPrice){
            return false;
        }
        else{
            return true;
        }
    }

    public String ToString(){
        return super.ToString() + ", bad habits: " + this.BadHabits.toLowerCase();
    }


    @Override
    public String GetName() {
        return null;
    }

    @Override
    public Double GetPrice() {
        return null;
    }
}
