import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class Dog extends Animal {
    public LocalDate LastWalk;
    public LocalDate getLastWalk(){return LastWalk;}
    private void setLastWalk(LocalDate value){LastWalk = value;}

    public double startPrice = 500;
    public int counter = 0;
    public List<Dog> dogs = new ArrayList<Dog>();

    public boolean NeedsWalk(){
        return (LocalDate.now().compareTo(this.LastWalk)>0);
    }

    public Dog(String name, Gender gender){
        super(name, gender);
        this.LastWalk = LocalDate.now();
    }

    public boolean CalculateDogPrice(){
        for (Dog dog : dogs)
        {
            dogs.get(counter).startPrice = 500;
            counter += 1;
            dogs.get(counter).startPrice -= 50;
        }

        if(this.startPrice < 35 && this.startPrice > 500){
            return false;
        }
        else{
            return true;
        }
    }

    public String ToString(){
        return super.ToString() + ", Last Walk: " + this.LastWalk.toString();
    }


    @Override
    public String GetName() {
        return null;
    }

    @Override
    public Double GetPrice() {
        return null;
    }
}
