public interface ISellable {
    public String GetName();
    public Double GetPrice();
}
