public class Webshop {
    public String Product;
    public String getProduct(){return Product;}
    private void setProduct(String value){Product = value;}

    public Double Price;
    public Double getPrice(){return  Price;}
    private void setPrice(Double value){Price = value;}

    public Webshop(String product, Double price){
        this.Product = product;
        this.Price = price;
    }

}
